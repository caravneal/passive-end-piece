function E = stiffHuman(model)

% unpack parameters
Q = model.disc.Q;

% stiffness parameters
Ed = 2.2e-21;
Ep = 8e-21;
sd = 3.9e-5;
L  = 6e-5;

% calculate arclength
s  = linspace(0,L,Q+1);
sm = (s(1:end-1)+s(2:end))/2;

% calculate stiffness
for jj=1:length(sm)
    if (sm(jj)>sd)
        E(jj) = Ed;
    else
        E(jj) = (Ep-Ed)*((sm(jj)-sd)/sd)^2+Ed;
    end
end

% non-dimensionlise
E = E/Ed;

end
