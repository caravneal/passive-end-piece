function [m] = momWormCutoff(t,model)

% unpack structure
k   = model.swimmer.k;
l   = model.swimmer.l;
Q   = model.disc.Q;

% calculate arclength at segment joints and midpoints
ds  = 1/Q;
s   = 0:ds:1;
sm  = (s(2:end) + s(1:end-1))/2;

% for each sm calculate the analytical integral of the active moment term 
% from sm to 1
for ii=1:Q
    if(sm(ii)<l)
        m(ii) = 1/k*(sin(k*l-t) - sin(k*sm(ii)-t));
    else
        m(ii) = 0;
    end
end
m = m';

end