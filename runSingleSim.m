clear all; close all; clc;

% add function paths
addpath(genpath('./'));

% create swimming structure
model = CreateModelStructure(13.5,4*pi,0.02,'varying',1);
  
% print swimming parameters
PrintSwimmingParameters(model);
  
% generate initial condition
Y0     = InitCondPresolve(model);

% solve problem
output = SolveSwimmingProblem(model.tmax,Y0,model,1);

% evaluate solution
Y      = deval(output.sol,output.tps);