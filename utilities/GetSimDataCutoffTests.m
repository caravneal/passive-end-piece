function [Y,VAL,W] = GetSimDataCutoffTests(calS,k,m0,lvec,Q,quadDisc,tracDisc,type)

switch type
    case 'varying'
        m0 = m0*(18/calS)*(k/(3*pi));
    case 'constant'
        m0 = m0*(18/calS)^4*(k/(3*pi))^2;
end

for ii=1:length(lvec)
    
    fn =['calS=',num2str(calS),'_k=',num2str(k),'_m0=',num2str(m0),...
        '_l=',num2str(lvec(ii)),'_Q=',num2str(Q),'_NQuad=',...
        num2str(6*quadDisc^2),'_NTrac=',num2str(6*tracDisc^2),'_',type];
    
    load(['data/',fn,'.mat']);
    
    testsVAL(ii) = VAL;
    testsW(ii) = W;
    testsY(:,:,ii) = Y;
    
end
VAL = testsVAL;
W = testsW;
Y = testsY;

end