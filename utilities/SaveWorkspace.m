function SaveWorkspace(Y,output,model)

% find coordinates
for t = 1:model.nt
    clf;
    hold on;
    
    % get coordinate data
    [head(t),flag(t),~] = GetProblemData(Y(:,t),model);   
end

% generate filename
fn = GenerateFilename(model);

% save workspace
fnData = ['data/',fn,'.mat'];
save(fnData,'Y','output','model','head','flag');

fprintf('Workspace saved.\n');

end