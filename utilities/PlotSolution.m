function PlotSolution(Y,model)

figure;

subplot(1,2,1);
for t = 1:20:model.nt
    
    hold on;
    
    % get coordinate data
    [head,flag,~] = GetProblemData([0;0;Y(3:end,t)],model);
    
    % plot
    plot(flag.x(:),flag.y(:),'LineWidth',1.5);
    axis equal;
    box on;
    xlabel('$x$','Interpreter','latex');
    ylabel('$y$','Interpreter','latex');
    
end
hold off;

subplot(1,2,2)
[S,T] = meshgrid(linspace(0,1,model.disc.Q),linspace(0,model.tmax,model.nt));
pcolor(T,S,(Y(3:end,:)-Y(3,:))')
shading interp;
xlabel('$t$','Interpreter','latex')
ylabel('$s$','Interpreter','latex')
colorbar;
box on;

end