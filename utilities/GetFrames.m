function frames = GetFrames(Y,output,model)

figure('visible','off'); box on;

% unpack stucture
x0 = model.swimmer.x0;

% get frames
for t = 1:model.nt
    clf;
    hold on;
    
    % get coordinate data
    [head,flag,~] = GetProblemData(Y(:,t),model);
    
    scatter3(head.xTrac(:),head.yTrac(:),head.zTrac(:),'k.');
    plot3(flag.x(:),flag.y(:),flag.z(:),'k-','LineWidth',1.5);
    
    hold off;
    title(sprintf('t = %g',output.tps(t)));
    axis('equal');
    xlabel('x');
    ylabel('y');
    view(2);
    
    axis([x0(1)-0.5 x0(1)+1.5 x0(2)-0.5 x0(2)+0.5]);
    
    % save frame data
    frame           = getframe(gcf);
    frames.data(t)  = frame;
    
end

fprintf('Frames calculated.\n');

end