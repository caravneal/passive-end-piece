function [Y,VAL,W] = GetSimDatam0Tests(calS,k,m0vec,l,Q,quadDisc,tracDisc,type,varargin)

switch type
    case 'varying'
        m0vec = m0vec.*(18/calS).*(k/(3*pi));
    case 'constant'
        m0vec = m0vec.*(18/calS)^4.*(k/(3*pi))^2;
end

for ii=1:length(m0vec)
    
    fn =['calS=',num2str(calS),'_k=',num2str(k),'_m0=',num2str(m0vec(ii)),...
        '_l=',num2str(l),'_Q=',num2str(Q),'_NQuad=',...
        num2str(6*quadDisc^2),'_NTrac=',num2str(6*tracDisc^2),'_',type];
    
    if (isempty(varargin{1}) == 1)
        fnData = ['data/',fn,'.mat'];
    else
        fnData =  ['data/',char(varargin{1}),'/',fn,'.mat'];
    end

    load(fnData);
    
    testsVAL(ii) = VAL;
    testsW(ii) = W;
    testsY(:,:,ii) = Y;
    
end
VAL = testsVAL;
W = testsW;
Y = testsY;

end