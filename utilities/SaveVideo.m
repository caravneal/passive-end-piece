function SaveVideo(frames,fps,fn)

fnVideo         = ['videos/',fn];
currentDir      = cd;

vid             = VideoWriter(fnVideo,'Motion JPEG AVI');
vid.FrameRate   = fps;
vid.Quality     = 100;
open(vid);
writeVideo(vid,frames.data);
close(vid);
fprintf('Video saved.\n')
cd(currentDir);

end
