function [FB] = ConstructForceBalance(head,flag,model,NN)

% unpack structure
Q     = model.disc.Q;
NTrac = head.NTrac;
ds    = flag.ds;

% construct total force balance rows
fb = sum(NN,1);
FB = [-fb(1:NTrac)   -ds*ones(1,Q) zeros(1,NTrac)       zeros(1,Q)    zeros(1,NTrac+Q) ;
      zeros(1,NTrac) zeros(1,Q)    -fb(NTrac+1:2*NTrac) -ds*ones(1,Q) zeros(1,NTrac+Q)];
  
end