function [head] = GetHeadData(Y,model) 

% unpack structure
a = [model.swimmer.a1,model.swimmer.a2,model.swimmer.a3];

% get angles
th = Y(3:end);

% generate spherical heads for quad and trac discretisation
head.XQuad = GenerateSphereDisc(model.disc.quad,1);
head.XTrac = GenerateSphereDisc(model.disc.trac,1);

head.XQuad = reshape(head.XQuad',[],3);
head.XTrac = reshape(head.XTrac',[],3);

% transofrm head to ellipsoid shape
head.XQuad = head.XQuad.*a;
head.XTrac = head.XTrac.*a;

% calculate rotation matrix required to correctly orientate headc
Rot        = [cos(th(1)) -sin(th(1))  0 ;
              sin(th(1))  cos(th(1))  0 ;
              0           0           1];
          
% rotate head points to be inline with flagellum
head.XQuad = (Rot*head.XQuad')';
head.XTrac = (Rot*head.XTrac')';         

% translate head to join flagellum
head.XQuad = head.XQuad - (Rot * [a(1);0;0] - [Y(1); Y(2); 0])';
head.XTrac = head.XTrac - (Rot * [a(1);0;0] - [Y(1); Y(2); 0])';

head.xQuad = head.XQuad(:,1);
head.yQuad = head.XQuad(:,2);
head.zQuad = head.XQuad(:,3);

head.xTrac = head.XTrac(:,1);
head.yTrac = head.XTrac(:,2);
head.zTrac = head.XTrac(:,3);

% if the flagellum joins the head at a point in the head discretisation,
% delete this point through finding its index
if (mod(model.disc.quad,2) ~= 0)
    diffQuad       = vecnorm([head.xQuad(:),head.yQuad(:),head.zQuad(:)] - [Y(1),Y(2),0],2,2);
    [~,iQ]         = min(diffQuad);
    head.xQuad(iQ) = [];
    head.yQuad(iQ) = [];
    head.zQuad(iQ) = [];
    head.XQuad     = [head.xQuad(:);head.yQuad(:);head.zQuad(:)];
else
    head.XQuad     = [head.xQuad(:);head.yQuad(:);head.zQuad(:)];
end

if (mod(model.disc.trac,2) ~= 0)
    diffTrac       = vecnorm([head.xTrac(:),head.yTrac(:),head.zTrac(:)] - [Y(1),Y(2),0],2,2);
    [~,iT]         = min(diffTrac);
    head.xTrac(iT) = [];
    head.yTrac(iT) = [];
    head.zTrac(iT) = [];
    head.XTrac     = [head.xTrac(:);head.yTrac(:);head.zTrac(:)];
else
    head.XTrac     = [head.xTrac(:);head.yTrac(:);head.zTrac(:)];
end

% calculate number of head points in both discretisations
head.NQuad = length(head.xQuad);
head.NTrac = length(head.xTrac);

end