function [Y,VAL,W,output,model,simStat] = CutoffSingleSim(Y0,calS,k,m0,type,l,varargin)

% create model structure
model = CreateModelStructure(calS,k,m0,type,l);
switch type
    case 'varying'
        model.swimmer.m0 = model.swimmer.m0*(18/calS)*(k/(3*pi));
    case 'constant'
        model.swimmer.m0 = model.swimmer.m0*(18/calS)^4*(k/(3*pi))^2;
end

try
    % solve problem
    output = SolveSwimmingProblem(model.tmax,Y0,model,0);
    
    % evaluate solution
    Y = deval(output.sol,output.tps);
    
    % calculate VAL
    VAL = CalculateVAL(Y,output.tps);
    
    % calculate work done over one period
    W = WorkDoneOverPeriod(output.tps,Y,model);
    
    % set simulation status to successful
    simStat = 1;
    
    fprintf(['Simulation complete for l=',num2str(l),'.\n']);
    
catch
    
    % set simulation status to failed
    simStat = 0;
    Y = [];
    VAL = [];
    W = [];
    
    fprintf(['Simulation failed for l=',num2str(l),'.\n']);
end

% save data
fn = GenerateFilename(model);

if (isempty(varargin{1}) == 1)
    fnData = ['data/',fn,'.mat'];
else
    if ~exist(['data/',char(varargin{1})], 'dir')
        mkdir(['data/',char(varargin{1})]);
    end
    fnData =  ['data/',char(varargin{1}),'/',fn,'.mat'];
end
save(fnData,'Y','output','model','VAL','W','simStat');

end