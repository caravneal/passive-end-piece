function [head,flag,NN] = GetProblemData(Y,model)

% get flagellum coordinates
flag = GetFlagellumData(Y,model);

% get head coordinates
head = GetHeadData(Y,model);

% construct nearest-neighbour matrix
NN   = NearestNeighbourMatrix(head.XQuad(:),head.XTrac(:));

end