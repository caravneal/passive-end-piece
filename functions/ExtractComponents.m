function [x1,x2,x3] = ExtractComponents(X)

N  = length(X)/3;
x1 = X(1:N);
x2 = X(N+1:2*N);
x3 = X(2*N+1:3*N);

end 