function [AK] = ConstructKinematicsMatrix(head,flag,model)

% unpack structure
Q     = model.disc.Q;
ang   = model.swimmer.ang;
NTrac = head.NTrac;
ds    = flag.ds;
th    = flag.th;

% calculate distances between head traction nodes and head-flagellum joint
Dx  = head.xTrac - flag.x(1);
Dy  = head.yTrac - flag.y(1);

% calculate x and y kinematic blocks
AKx = tril(repmat(-ds*sin(th(:)+ang)',Q,1),-1)+ diag(-ds/2*sin(th(:)+ang)');
AKy = tril(repmat(ds*cos(th(:)+ang)',Q,1),-1)+ diag(ds/2*cos(th(:)+ang)');

% construct kinematics matrix
AK  = [ ones(NTrac,1)  zeros(NTrac,1) -Dy(:) zeros(NTrac,Q-1);
        ones(Q,1)      zeros(Q,1)      AKx;
        zeros(NTrac,1) ones(NTrac,1)   Dx(:) zeros(NTrac,Q-1);
        zeros(Q,1)     ones(Q,1)       AKy;
        zeros(NTrac+Q,Q+2) ];
    
end
