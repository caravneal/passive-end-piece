function [output] = SolveSwimmingProblem(tmax,Y0,model,odeProgress)

% define swimming rates function
dY = @(t,Y) CalculateSwimmingRates(t,Y,model);

% solve swimming problem using ode15s
if (odeProgress == 1)
    odeOptions = odeset('OutputFcn',@odetpbar);
    output.sol = ode15s(dY,[0,tmax],Y0,odeOptions);
else
    output.sol = ode15s(dY,[0,tmax],Y0);
end

output.tps = linspace(0,model.tmax,model.nt);

end