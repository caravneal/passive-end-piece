function [VAL] = CalculateVAL(Y,t)

% find time corresponding to the start and end of a beat
T = 2*pi;
m = mod(t,T);
m = m(m>0);

% find time indices corresponding to the start and end of a beat
[~,ind] = find(m < 1.35e-1);

% find position of x0 at the start and end of the beat (once stabilised)
x02 = [Y(1,ind(4)),Y(2,ind(4))];
x01 = [Y(1,ind(3)),Y(2,ind(3))];

% calculate VAL
VAL = norm(x02-x01)/T;

end