function [AE] = ConstructElasticsMatrix(head,flag,model,NN)

% unpack structure
Q     = model.disc.Q;
calS  = model.swimmer.calS;
NTrac = head.NTrac;
ds    = flag.ds;

% calculate total moment balance row
[MB] = ConstructMomentBalance(head,flag,model);

% calculate total force balance rows
[FB] = ConstructForceBalance(head,flag,model,NN);

% calculate elastics block
EB  = -[ triu(repmat( ds*flag.y(1:Q),1,Q)) + triu(repmat(-ds*flag.ym',Q,1))...
    triu(repmat(-ds*flag.x(1:Q),1,Q)) + triu(repmat( ds*flag.xm',Q,1))];

% multiply by sperm number (and bending stiffness, depending on type)
switch model.swimmer.stiffness
    case 'varying'
        
        % calculate stiffness function
        E   = model.swimmer.stiffFunc(model);
        
        EBx = calS^4./E(2:end)'.*EB(2:end,1:Q);
        EBy = calS^4./E(2:end)'.*EB(2:end,Q+1:2*Q);
        
    case 'constant'
        EBx = calS^4*EB(2:end,1:Q);
        EBy = calS^4*EB(2:end,Q+1:2*Q);
end

EB = [zeros(Q-1,NTrac) EBx zeros(Q-1,NTrac) EBy zeros(Q-1,NTrac+Q)];

% form full elastics matrix
AE = [MB; EB; FB];

end