function [MB] = ConstructMomentBalance(head,flag,model)

% unpack structure
ds    = flag.ds;
Q     = model.disc.Q;
NTrac = head.NTrac;

% calculate distances between head/flagellum points and head-flagellum joint
DFx   = ds*(flag.xm - flag.x(1))';
DFy   = ds*(flag.ym - flag.y(1))';
DHx   = (head.xTrac - flag.x(1))';
DHy   = (head.yTrac - flag.y(1))';

% form total moment balance row
MB    = [ -DHy -DFy DHx DFx zeros(1,NTrac+Q)];

end