function [W] = CalculateWorkDone(t,Y,model)

% get all problem data
[head,flag,NN] = GetProblemData(Y,model);

% construct hydrodynamics block
AH = ConstructHydrodynamicsMatrix(head,flag,model,NN);

% construct kinematics block
AK = ConstructKinematicsMatrix(head,flag,model);

% construct elastodynamics block
AE = ConstructElasticsMatrix(head,flag,model,NN);

% calculate active moment integral
b  = ConstructRHS(t,head,flag,model);

% construct linear system (matrix and rhs vector)
AZ = zeros(size(AE,1),size(AK,2));
A  = [AZ AE ; AK AH];

% solve system
dZ = A\b;

% extract solution dY=[dx0dt dy0dt dth1dt dth2dt ... dthQdt]
dY = dZ(1:model.disc.Q+2);

% calculate work done
F  = dZ((model.disc.Q+3):end);
dX = AK*dY;
W  = F'*dX;

end