function X = TranslatePoints(X,m)

[x1,x2,x3] = ExtractComponents(X);
X          = [x1+m(1); x2+m(2); x3+m(3)];

end

