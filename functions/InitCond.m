function [Y0] = InitCond(model)

% unpack structure
k  = model.swimmer.k;
Q  = model.disc.Q;
x0 = model.swimmer.x0;

% generate refined initial condition
x  = linspace(0,1,1e6);
x  = x(:);
Nx = length(x);
y  = (1/k)*(sin(k)*ones(Nx,1) - sin(k*x));

% spline to get correct number of segments
ds      = sqrt((x(2:end) - x(1:end-1)).^2 + (y(2:end) - y(1:end-1)).^2);
s       = [0; cumsum(ds)];
delta   = max(s)/Q;
ss      = 0:delta:max(s); 
 
yy      = spline(s,y,ss);
xx      = spline(s,x,ss);

% calculate segment theta values
dy      = diff(yy);
dx      = diff(xx);
th      = atan(dy./dx); 

% form initial condition
Y0      = [x0(1); x0(2); th(:)];

end