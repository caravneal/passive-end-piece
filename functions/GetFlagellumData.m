function [flag] = GetFlagellumData(Y,model)

% unpack structure
Q   = model.disc.Q;
x0  = model.swimmer.x0;
ang = model.swimmer.ang;

% get angles
flag.th = Y(3:end);

% get arclength parameters
flag.ds = 1/Q;
flag.s  = 0:flag.ds:1;
flag.sm = (flag.s(2:end) + flag.s(1:end-1))/2;

% get flagellum endpoint coordinates in the body frame
flag.x(1) = 0;
flag.y(1) = 0;
flag.z    = x0(3)*ones(Q+1,1);

for ii=1:Q
   flag.x(ii+1,1) = flag.x(ii) + flag.ds*cos(flag.th(ii)); 
   flag.y(ii+1,1) = flag.y(ii) + flag.ds*sin(flag.th(ii)); 
end

flag.X = [flag.x(:); flag.y(:); flag.z(:)];

% get flagellum midpoint coordinates in the body frame
flag.xm = flag.x(1:Q) + flag.ds/2*cos(flag.th);
flag.ym = flag.y(1:Q) + flag.ds/2*sin(flag.th);
flag.zm = x0(3)*ones(Q,1);

flag.Xm = [flag.xm(:); flag.ym(:); flag.zm(:)];

% calculate rotation matrix required to transform to lab frame
Rot     = [cos(ang) -sin(ang) 0 ;
           sin(ang)  cos(ang) 0 ;
           0         0        1];
      
% transform all points to lab frame
flag.X  = ApplyRotationMatrix(Rot,flag.X);
flag.Xm = ApplyRotationMatrix(Rot,flag.Xm);

flag.X  = TranslatePoints(flag.X,[Y(1);Y(2);0]);
flag.Xm = TranslatePoints(flag.Xm,[Y(1);Y(2);0]);

[x,y,z]    = ExtractComponents(flag.X);
[xm,ym,zm] = ExtractComponents(flag.Xm);

flag.x  = x;  flag.y  = y;   flag.z = z;
flag.xm = xm; flag.ym = ym; flag.zm = zm;

end