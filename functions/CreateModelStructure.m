function model = CreateModelStructure(calS,k,m0,type,l)

% create structure
model = struct( ...
        'epsilon', 0.01,                 ... % regularisation parameter
        'tmax'   , 8*pi,                 ... % maximum time to simulate to
        'nt'     , 200 ,                 ... % numver of time points
        'disc'   ,                       ... % discretisation parameters
            struct( ...
            'quad', 16 ,                 ... % quadrature discretisation
            'trac', 8 ,                  ... % traction discretisation
            'Q'   , 60                   ... % number of segments
            ),                           ...
        'swimmer',                       ... % swimmer parameters
            struct( ...
            'x0'       , [0;0;0]    ,    ... % initial head-flagellum point
            'a1'       , 2.0/45     ,    ... % head axis length 1
            'a2'       , 1.6/45     ,    ... % head axis length 2
            'a3'       , 1.0/45     ,    ... % head axis length 3
            'ang'      , 0          ,    ... % cell orientation angle
            'stiffness', type       ,    ... % stiffness type
            'momFunc'  , @momWormCutoff, ... % active moment function
            'stiffFunc', @stiffHuman,    ... % stiffness function
            'calS'     , calS       ,    ... % dimensionless swimming group
            'm0'       , m0         ,    ... % active moment strength
            'k'        , k          ,    ... % ative moment wavenumber
            'l'        , l               ... % cutoff for actuation
            )                            ...
        );

end