function [W] = WorkDoneOverPeriod(t,Y,model)

% find time corresponding to the start and end of a beat
T = 2*pi;
m = mod(t,T);
m = m(m>0);

% find time indices corresponding to the start and end of a beat
[~,ind] = find(m < 1.35e-1);

indvec = ind(3):ind(4);

for kk = indvec(1):indvec(end)
    W = CalculateWorkDone(t(kk),Y(:,kk),model);
end

end