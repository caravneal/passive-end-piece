function [AH] = ConstructHydrodynamicsMatrix(head,flag,model,NN)

% unpack structure
th  = flag.th;
ds  = flag.ds;
ang = model.swimmer.ang;
Q   = model.disc.Q;
eps = model.epsilon;

% Rotation matrices for each of the flagella segments
Rot   = [cos(th'+ang) -sin(th'+ang) zeros(1,Q);
         sin(th'+ang)  cos(th'+ang) zeros(1,Q); 
         zeros(1,Q)    zeros(1,Q)   ones(1,Q)];
     
% calculate various hydrodynamics matrices - some can be done analytically
HH  = RegStokesletsDJS(head.XTrac,head.XQuad,eps);
FH  = RegStokesletsDJS(flag.Xm,head.XQuad,eps);
HF  = RegStokesletAnalyticIntegrals(head.XTrac,flag.Xm,ds/2,Rot,eps);
FF  = RegStokesletAnalyticIntegrals(flag.Xm,flag.Xm,ds/2,Rot,eps);

% multiply by nearest-neighbour matrix
HH  = HH*NN;
FH  = FH*NN;

% calculate dimension of various matrix blocks 
DHH = size(HH)/3;  
DFH = size(FH)/3; 
DHF = size(HF)/3; 
DFF = size(FF)/3; 

% calculate various blocks of the full hydrodynamics matrix by pulling
% apart the calculated matrices and combining appropriately
Hxx = [ HH(1:DHH(1),1:DHH(2)) HF(1:DHF(1),1:DHF(2)) ;
        FH(1:DFH(1),1:DFH(2)) FF(1:DFF(1),1:DFF(2))];

Hxy = [ HH(1:DHH(1),DHH(2)+1:2*DHH(2)) HF(1:DHF(1),DHF(2)+1:2*DHF(2)) ;
        FH(1:DFH(1),DFH(2)+1:2*DFH(2)) FF(1:DFF(1),DFF(2)+1:2*DFF(2))];
    
Hxz = [ HH(1:DHH(1),2*DHH(2)+1:3*DHH(2)) HF(1:DHF(1),2*DHF(2)+1:3*DHF(2)) ;
        FH(1:DFH(1),2*DFH(2)+1:3*DFH(2)) FF(1:DFF(1),2*DFF(2)+1:3*DFF(2))];
        
Hyx = [ HH(DHH(1)+1:2*DHH(1),1:DHH(2)) HF(DHF(1)+1:2*DHF(1),1:DHF(2)) ;
        FH(DFH(1)+1:2*DFH(1),1:DFH(2)) FF(DFF(1)+1:2*DFF(1),1:DFF(2))];
    
Hyy = [ HH(DHH(1)+1:2*DHH(1),DHH(2)+1:2*DHH(2)) HF(DHF(1)+1:2*DHF(1),DHF(2)+1:2*DHF(2)) ;
        FH(DFH(1)+1:2*DFH(1),DFH(2)+1:2*DFH(2)) FF(DFF(1)+1:2*DFF(1),DFF(2)+1:2*DFF(2))];
    
Hyz = [ HH(DHH(1)+1:2*DHH(1),2*DHH(2)+1:3*DHH(2)) HF(DHF(1)+1:2*DHF(1),2*DHF(2)+1:3*DHF(2)) ;
        FH(DFH(1)+1:2*DFH(1),2*DFH(2)+1:3*DFH(2)) FF(DFF(1)+1:2*DFF(1),2*DFF(2)+1:3*DFF(2))];
    
Hzx = [ HH(2*DHH(1)+1:3*DHH(1),1:DHH(2)) HF(2*DHF(1)+1:3*DHF(1),1:DHF(2)) ;
        FH(2*DFH(1)+1:3*DFH(1),1:DFH(2)) FF(2*DFF(1)+1:3*DFF(1),1:DFF(2))];
    
Hzy = [ HH(2*DHH(1)+1:3*DHH(1),DHH(2)+1:2*DHH(2)) HF(2*DHF(1)+1:3*DHF(1),DHF(2)+1:2*DHF(2)) ;
        FH(2*DFH(1)+1:3*DFH(1),DFH(2)+1:2*DFH(2)) FF(2*DFF(1)+1:3*DFF(1),DFF(2)+1:2*DFF(2))];
    
Hzz = [ HH(2*DHH(1)+1:3*DHH(1),2*DHH(2)+1:3*DHH(2)) HF(2*DHF(1)+1:3*DHF(1),2*DHF(2)+1:3*DHF(2)) ;
        FH(2*DFH(1)+1:3*DFH(1),2*DFH(2)+1:3*DFH(2)) FF(2*DFF(1)+1:3*DFF(1),2*DFF(2)+1:3*DFF(2))];
    
AH  = -[ Hxx Hxy Hxz; Hyx Hyy Hyz; Hzx Hzy Hzz];

end