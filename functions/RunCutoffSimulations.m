function [Y,VAL,W,output,model,simStat] = RunCutoffSimulations(calS,k,m0,type,lvec,varargin)

close all; warning off;

fprintf(['%%%% Cutoff test for ',type,' stiffness \nwith calS=',...
    num2str(calS),' k=',num2str(k),' %%%%\n\n']);

% create model structure
model = CreateModelStructure(calS,k,m0,type,1);
switch type
    case 'varying'
        model.swimmer.m0 = model.swimmer.m0*(18/calS)*(k/(3*pi));
    case 'constant'
        model.swimmer.m0 = model.swimmer.m0*(18/calS)^4*(k/(3*pi))^2;
end

% generate initial condition based on l=1
try
    Y0 = InitCondPresolve(model);
    errCode = 0;
catch
    
    % exit code if initial condition could not be calculated
    fprintf('Calculation of initial condition failed. Exiting code...\n');
    errCode = 1;
end

if errCode == 0
    if length(lvec) == 1
        [Y,VAL,W,output,model,simStat] = CutoffSingleSim(Y0,calS,k,m0,type,lvec,varargin);
    else
        parfor ii=1:length(lvec)
            [Y(:,:,ii),VAL(ii),W(ii),output(ii),model(ii),simStat(ii)] = ...
                CutoffSingleSim(Y0,calS,k,m0,type,lvec(ii),varargin);
        end
    end
else
    
    % set variables as empty
    Y = [];
    VAL = [];
    W = [];
    output = [];
    model = [];
    simStat = 0;
end

end