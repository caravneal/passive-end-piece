function [XRot] = ApplyRotationMatrix(Rot,X)

[x1,x2,x3] = ExtractComponents(X);
XRot       = Rot * [x1'; x2'; x3']; 
XRot       = [XRot(1,:)'; XRot(2,:)'; XRot(3,:)'];

end
