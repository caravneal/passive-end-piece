function [Y0] = InitCondPresolve(model)

% generate naive initial condition
Y0 = InitCond(model);

% solve swimming problem over one beat for an improved initial condition
output = SolveSwimmingProblem(2*pi,Y0,model,0);
Y0     = deval(output.sol,2*pi);

fprintf('Initial condition presolve complete.\n');

end