clear all; close all; clc;

% add function paths
addpath(genpath('./'));

% folder name for data to be saved
fldName = 'cutoff-tests';

% define l values
lvec  = linspace(0,1,61); lvec  = lvec(end);

% calS = 18 tests
RunCutoffSimulations(18,3*pi,0.01,'varying',lvec,fldName);
RunCutoffSimulations(18,4*pi,0.01,'varying',lvec,fldName);
RunCutoffSimulations(18,5*pi,0.01,'varying',lvec,fldName);

% calS = 13.5 tests
RunCutoffSimulations(13.5,3*pi,0.01,'varying',lvec,fldName);
RunCutoffSimulations(13.5,4*pi,0.01,'varying',lvec,fldName);
RunCutoffSimulations(13.5,5*pi,0.01,'varying',lvec,fldName);

% calS = 9 tests
RunCutoffSimulations(9,3*pi,0.01,'varying',lvec,fldName);
RunCutoffSimulations(9,4*pi,0.01,'varying',lvec,fldName);
RunCutoffSimulations(9,5*pi,0.01,'varying',lvec,fldName);